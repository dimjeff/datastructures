/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day03customarraysort;

import java.util.Arrays;
import java.util.Date;
import java.util.Random;

/**
 *
 * @author ipd
 */
public class Day03CustomArraySort {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //int[] data = {582, 996, 161, 174, 708, 609, 278, 365, 437, 388, 338, 151, 18, 843, 620, 593, 524, 770, 647, 549};
        int d = 50;
        int[] data = new int[d];
        for(int i=0;i<d;i++){
            Random r = new Random();
            int j = r.nextInt(999);
            data[i] = j;
        }
        shellSort(data);
        int w = 0;
        boolean sort = false;
        do {
            sort = false;
            for (int i = 0; i < data.length - 1; i++) {
                if (data[i] > data[i + 1]) {
                    int tem = data[i];
                    data[i] = data[i + 1];
                    data[i + 1] = tem;
                    sort = true;
                    w++;
                }
            }
        } while (sort);
        System.out.println(String.format("%s : %d", Arrays.toString(data), w));
    }

    static void shellSort(int[] arr) {
        int[] newd = new int[arr.length];
        for(int f=0;f<arr.length;f++){
            newd[f]=arr[f];
        }
        System.out.println(Arrays.toString(newd));
        int len = newd.length;
        int w = 0;
        for (int gap = (int) (double) len / 2; gap > 0; gap = (int) (double) gap / 2) {
            // 注意：这里和动图演示的不一样，动图是分组执行，实际操作是多个分组交替执行
            for (int i = gap; i < len; i++) {
                int j = i;
                int current = newd[i];
                while (j - gap >= 0 && current < newd[j - gap]) {
                    newd[j] = newd[j - gap];
                    j = j - gap;
                    w++;
                }
                newd[j] = current;
            }
        }
        System.out.println(Arrays.toString(newd));
        System.out.println(w);
    }
}
