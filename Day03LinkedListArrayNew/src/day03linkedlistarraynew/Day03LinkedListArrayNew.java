/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day03linkedlistarraynew;

/**
 *
 * @author trakadmin
 */
public class Day03LinkedListArrayNew {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        LinkedListArrayOfStrings link = new LinkedListArrayOfStrings();
        for(int i=0;i<50;i++)
            link.add("AAA"+i);
        System.out.println(link);
    }

}

class LinkedListArrayOfStrings {

    private class Container {

        Container next;
        String value;

        public Container(String value) {
            this.value = value;
        }
    }
    Container start, end;
    int size;

    public void add(String value) {
        Container newcont = new Container(value);
        
        if(start == null){
            start = newcont;
            end = newcont;
        }else{
            end.next = newcont;
            end = newcont;
        }
        size ++;
    }

    public String get(int index) {
        if(index<0 || index>size-1){
            throw new IndexOutOfBoundsException();
        }
        Container cont = start;
        int count = 0;
        while(cont!=null){
            if(count == index)
                return cont.value;
            cont = cont.next;
            count++;
        }
        return "";
    }

    public void insertValueAtIndex(int index, String value) {
        throw new UnsupportedOperationException();
    }

    public void deleteByIndex(int index) {
        throw new UnsupportedOperationException();
    }

    public boolean deleteByValue(String value) {
        throw new UnsupportedOperationException();
    } // delete first value found

    public int getSize() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String toString() {
        return String.format("[%s]", String.join(",", toArray()));
    } // comma-separated values list

    public String[] toArray() {
        String[] result = new String[size];
        int count = 0;
        Container current = start;
        while (current != null) {
            result[count++] = current.value;
            current = current.next;
        }
        return result;
    } // could be used for Unit testing

}
