
import java.awt.Graphics;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author trakadmin
 */
public class Canvas extends javax.swing.JPanel {

    /**
     * Creates new form Canvas
     */
    public Canvas() {
        initComponents();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        //g.drawLine(10,10,getWidth()-10,10);
        drawLine(10,50,getWidth()-10,50,g);
    }
    
    void drawLine(int fromX,int fromY,int toX, int toY,Graphics g){
        if(toX-fromX<5)return;
        g.drawLine(fromX, toY, fromX + (int)Math.floor((toX-fromX)/3.0), toY);
        drawLine(fromX, toY+50, fromX + (int)Math.floor((toX-fromX)/3.0), toY+50,g);
        g.drawLine(toX - (int)Math.floor((toX-fromX)/3.0), toY, toX, toY);
        drawLine(toX - (int)Math.floor((toX-fromX)/3.0), toY+50, toX, toY+50,g);
    }    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
