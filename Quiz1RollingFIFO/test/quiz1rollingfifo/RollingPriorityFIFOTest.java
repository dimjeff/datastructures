/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz1rollingfifo;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ipd
 */
public class RollingPriorityFIFOTest {
    
    public RollingPriorityFIFOTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of enqueue method, of class RollingPriorityFIFO.
     */
    @Test(timeout = 1000)
    public void testEnqueue() throws FIFOFullException {
        //System.out.println("test enqueue");
        String value = "A";
        boolean priority = false;
        RollingPriorityFIFO instance = new RollingPriorityFIFO(5);
        instance.enqueue(value, priority);
        // TODO review the generated test code and remove the default call to fail.
        assertArrayEquals("To test enqueue: ",new String[]{"A"}, instance.toArray());
        instance.enqueue("B", false);
        assertArrayEquals("To test enqueue: ",new String[]{"B","A"}, instance.toArray());
        instance.enqueue("C", false);
        assertArrayEquals("To test enqueue: ",new String[]{"C","B","A"}, instance.toArray());
        instance.enqueue("D", false);
        assertArrayEquals("To test enqueue: ",new String[]{"D","C","B","A"}, instance.toArray());
        instance.enqueue("E", false);
        assertArrayEquals("To test enqueue: ",new String[]{"E","D","C","B","A"}, instance.toArray());
        //instance.enqueue("F", false);
        //assertArrayEquals("To test enqueue: ",new String[]{"E","D","C","B","A"}, instance.toArray());
    }

    @Test(timeout = 1000)
    public void testFIFOFullException() throws FIFOFullException {
        RollingPriorityFIFO instance = new RollingPriorityFIFO(5);
        try {
            instance.enqueue("A", false);
            instance.enqueue("R", false);
            instance.enqueue("G", false);
            instance.enqueue("U", false);
            instance.enqueue("W", false);
            instance.enqueue("X", false);
            assertTrue("Exception expected on enqueue X", false);
        } catch (FIFOFullException ex) {
            // all good
        }    
    }
    
    /**
     * Test of dequeue method, of class RollingPriorityFIFO.
     */
    @Test(timeout = 1000)
    public void testDequeue() throws FIFOFullException{
        RollingPriorityFIFO instance = new RollingPriorityFIFO(5);
        instance.enqueue("A", false);
        instance.enqueue("B", false);
        instance.enqueue("C", false);
        instance.enqueue("D", false);
        instance.enqueue("E", false);
        String expResult = "A";
        String result = instance.dequeue();
        assertEquals(expResult, result);
        expResult = "B";
        result = instance.dequeue();
        assertEquals(expResult, result);
        instance.enqueue("W", true);
        instance.enqueue("Z", false);
        result = instance.dequeue();
        assertEquals("W", result);
        result = instance.dequeue();
        assertEquals("C", result);
    }
//
//    /**
//     * Test of size method, of class RollingPriorityFIFO.
//     */
    @Test(timeout = 1000)
    public void testSize() throws FIFOFullException {
        RollingPriorityFIFO instance = new RollingPriorityFIFO(5);
        instance.enqueue("A", false);
        instance.enqueue("B", false);
        instance.enqueue("C", false);
        instance.enqueue("D", false);
        instance.enqueue("E", false);
        int expResult = 5;
        int result = instance.size();
        assertEquals(expResult, result);
    }
//
//    /**
//     * Test of sizeMax method, of class RollingPriorityFIFO.
//     */
    @Test(timeout = 1000)
    public void testSizeMax() throws FIFOFullException {
        RollingPriorityFIFO instance = new RollingPriorityFIFO(10);
        instance.enqueue("A", false);
        instance.enqueue("B", false);
        instance.enqueue("C", false);
        instance.enqueue("D", false);
        instance.enqueue("E", false);
        int expResult = 10;
        int result = instance.sizeMax();
        assertEquals(expResult, result);
    }

//    /**
//     * Test of toArray method, of class RollingPriorityFIFO.
//     */
    @Test(timeout = 1000)
    public void testToArray() throws FIFOFullException {
        RollingPriorityFIFO instance = new RollingPriorityFIFO(10);
        instance.enqueue("A", false);
        // TODO review the generated test code and remove the default call to fail.
        assertArrayEquals("To test ToArray: ",new String[]{"A"}, instance.toArray());
        instance.enqueue("B", false);
        assertArrayEquals("To test ToArray: ",new String[]{"B","A"}, instance.toArray());
        instance.enqueue("C", false);
        assertArrayEquals("To test ToArray: ",new String[]{"C","B","A"}, instance.toArray());
        instance.enqueue("D", false);
        assertArrayEquals("To test ToArray: ",new String[]{"D","C","B","A"}, instance.toArray());
        instance.enqueue("E", false);
        assertArrayEquals("To test ToArray: ",new String[]{"E","D","C","B","A"}, instance.toArray());
    }
//
//    /**
//     * Test of toArrayOnlyPriority method, of class RollingPriorityFIFO.
//     */
    @Test(timeout = 1000)
    public void testToArrayOnlyPriority() throws FIFOFullException {
        RollingPriorityFIFO instance = new RollingPriorityFIFO(10);
        instance.enqueue("A", false);
        // TODO review the generated test code and remove the default call to fail.
        assertArrayEquals("To test ToArrayOnlyPriority: ",new String[]{}, instance.toArrayOnlyPriority());
        instance.enqueue("B", true);
        assertArrayEquals("To test ToArrayOnlyPriority: ",new String[]{"B"}, instance.toArrayOnlyPriority());
        instance.enqueue("C", false);
        assertArrayEquals("To test ToArrayOnlyPriority: ",new String[]{"B"}, instance.toArrayOnlyPriority());
        instance.enqueue("D", true);
        assertArrayEquals("To test ToArrayOnlyPriority: ",new String[]{"D","B"}, instance.toArrayOnlyPriority());
        instance.enqueue("E", false);
        assertArrayEquals("To test ToArrayOnlyPriority: ",new String[]{"D","B"}, instance.toArrayOnlyPriority());
    }
//
//    /**
//     * Test of toString method, of class RollingPriorityFIFO.
//     */
    @Test(timeout = 1000)
    public void testToString() throws FIFOFullException {
        RollingPriorityFIFO instance = new RollingPriorityFIFO(10);
        instance.enqueue("A", false);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals("To test ToString: ","A", instance.toString());
        instance.enqueue("B", true);
        assertEquals("To test ToString: ","B,A", instance.toString());
        instance.enqueue("C", false);
        assertEquals("To test ToString: ","C,B,A", instance.toString());
        instance.enqueue("D", true);
        assertEquals("To test ToString: ","D,C,B,A", instance.toString());
        instance.enqueue("E", false);
        assertEquals("To test ToString: ","E,D,C,B,A", instance.toString());
    }
//    
}
