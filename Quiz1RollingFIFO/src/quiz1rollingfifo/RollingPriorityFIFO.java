/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz1rollingfifo;

/**
 *
 * @author ipd
 */
class FIFOFullException extends Exception {
    public FIFOFullException() {
    }
}

public class RollingPriorityFIFO {

    private class Item {

        // add constructor of your choice
        Item next,prev;
        boolean priority;
        String value;
        int debugId; // for debugging only
    }
    private Item start, end;
    private int itemsTotal, itemsCurrUsed;

    /* Parameter itemsTotal must be 5 or more, otherwise IllegalArgumentException
    * is thrown. Items are allocated and connected via next pointer only
    * once - here, in the constructor. After that they are re-used.
     */
    public RollingPriorityFIFO(int itemsTotal) {
        if (itemsTotal < 5) {
            throw new IllegalArgumentException("Size must be 5 or greater");
        }
        this.itemsTotal = itemsTotal;
        Item first = new Item();
        start = first;
        end = first;
        for (int i = 1; i < itemsTotal; i++) {
            Item item = new Item();
            item.next = first;
            first.prev = item;
            first = item;
        }
        first.prev = start;
        start.next = first;
        System.out.println("Items allocation complete");
    }

// Places value in the next available Item. If FIFO is full throws exception.
    public void enqueue(String value, boolean priority) throws FIFOFullException {
        if (itemsCurrUsed >= itemsTotal) {
            throw new FIFOFullException();
        }
        if(itemsCurrUsed == 0){
            start.value = value;
            start.priority = priority;
        } else if(start.next!=end){
            start = start.next;
            start.value = value;
            start.priority = priority;
        }else{
            throw new FIFOFullException();
        }
        itemsCurrUsed++;
    }

    /* returns null if fifo is empty, if it is not emtpy then
    * priority=true items are sarched first
    * if none is found then non-priority item is returned
     */
    public String dequeue() {
        if(itemsCurrUsed==0){
            return null;
            //throw new FIFOFullException("The queue is empty!");
        }
        // end always points to item to be dequeued
        if (itemsCurrUsed == 1) {
            String value = end.value;
            itemsCurrUsed--;
            end.value = null; // clean up, avoid memory leaks
            return value;
        } else {
            // find a priority item first
            Item current = end;
            for(int i=0;i<itemsCurrUsed;i++)
            {
                if (current.priority) { // found a priority item
                    String value = current.value;
                    current.value = null; // clean up the item
                    
                    if (current == start) { // removal at start
                        start = start.prev;
                    } else {
                        if (current == end) { // removing at end pointer
                            end = current.next;
                        }
                        current.prev.next = current.next; // dangerous
                        current.next.prev=current.prev;
                        //
                        start.next.prev = current;
                        current.next = start.next;
                        //
                        start.next = current;
                        current.prev = start;
                        //
                    }
                    itemsCurrUsed--;
                    return value;
                }
                current = current.next;
            }
            
            
//            Item previous = null;
//            for (Item current = end; current != start.next; current = current.next) {
//                if (current.priority) { // found a priority item
//                    String value = current.value;
//                    current.value = null; // clean up the item
//                    
//                    if (current == start) { // removal at start
//                        start = getPreviousOf(start);
//                    } else {
//                        if (previous == null) { // removing at end pointer
//                            end = current.next;
//                            previous = getPreviousOf(current);
//                        }
//                        previous.next = current.next; // dangerous
//                        //
//                        current.next = start.next;
//                        start.next = current;
//                        //
//                    }
//                    itemsCurrUsed--;
//                    return value;
//                }
//                previous = current;
//            }
            // otherwise return non-priority item
            String value = end.value;
            itemsCurrUsed--;
            end.value = null; // clean up, avoid memory leaks
            end = end.next; // move on to next item to be fetched
            return value;
        }
    }

    public int size() {
        return itemsCurrUsed;
    } // current FIFO size

    public int sizeMax() {
        return itemsTotal;
    } // maximum FIFO size
// Returns array of Strings of all items in FIFO.

    public String[] toArray() {
        String[] strArr = new String[itemsCurrUsed];
        Item item = end;
        for(int i=0;i<itemsCurrUsed;i++)
        {
            strArr[i] = item.value;
            item = item.next;
        }
        return strArr;
    }
// Returns array of String only of priority items in FIFO.

    public String[] toArrayOnlyPriority() {
        if (itemsCurrUsed == 0) {
            return new String[0];
        }
        // Iteration 1: count how many priority items are there
        int countPriority = 0;
        for (Item current = end; current != start.next; current = current.next) {
            if (current.priority) {
                countPriority++;
            }
        }
        // Iteration 2: collect the items and save them into the array
        String[] result = new String[countPriority];
        int i = 0;
        for (Item current = end; current != start.next; current = current.next) {
            if (current.priority) {
                result[i] = current.value;
                i++;
            }
        }
        return result;
    }
    // Items with priority=true have "*" appended, e.g. "[Jerry*,Maria,Tom*];
    // The item that get() will retrieve next is at the end.

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("[");
        int i = 0;
        for (Item current = end; current != start.next; current = current.next) {
            result.append((current == end) ? "" : ",");
            result.append(current.value);
            result.append(current.priority ? "*" : "");
            i++;
        }
        result.append("]");
        return result.toString();
    }

}

