/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day04simpletree;

/**
 *
 * @author trakadmin
 */
public class Day04SimpleTree {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        SimpleTreeOfUniqueStrings tree = new SimpleTreeOfUniqueStrings();
        tree.add("M");
        tree.add("A");
        tree.add("D");
        tree.add("Z");
        tree.add("J");
        tree.add("S");
        tree.add("B");
        tree.add("X");
        tree.printDebug();
        System.out.println(tree);
    }

}

class SimpleTreeOfUniqueStrings {

    private class Node {

        String value;
        Node left, right, parent;

        public Node(String value) {
            this.value = value;
        }
    }
    private Node root;
    private int nodesTotal;
    private String[] strArr;
    private int arrIndex=0;

    public void add(String value) {
        if (has(value)) {
            throw new IllegalArgumentException();
        }
        Node node = root;
        if (node == null) {
            root = new Node(value);
            nodesTotal++;
            return;
        }
        Node parent;
        while (node != null) {
            int compare = value.compareTo(node.value);
            parent = node;
            if (compare < 0) {
                node = node.left;
                if (node == null) {
                    parent.left = new Node(value);
                    parent.left.parent = parent;
                }
            } else {
                node = node.right;
                if (node == null) {
                    parent.right = new Node(value);
                    parent.right.parent = parent;
                }
            }
        }
        nodesTotal++;
        //throw new UnsupportedOperationException();
    } // throws IllegalArgumentException if value already present

    public boolean has(String value) {
        Node node = root;
        while (node != null) {
            if (node.value.equals(value)) {
                return true;
            }
            int compare = value.compareTo(node.value);
            if (compare == 0) {
                return true;
            } else if (compare < 0) {
                node = node.left;
            } else {
                node = node.right;
            }
        }
        return false;
        //throw new UnsupportedOperationException();
    }

    public void remove(String value) {
        throw new UnsupportedOperationException();
    } // HARD! throws IllegalArgumentException if value does not exist

    public int size() {
        throw new UnsupportedOperationException();
    }

    public void preTraverseBTree(Node node) {
        if (node != null) {
            strArr[arrIndex]=node.value;
            arrIndex++;
            preTraverseBTree(node.left);
            preTraverseBTree(node.right);
        }
    }
    
    public String[] toArray() {
        Node node = root;
        arrIndex = 0;
        strArr = new String[nodesTotal];
        preTraverseBTree(node);

        return strArr;
    } // allocate array of nodesTotal, recursively visit all nodes of the tree

    public String[] toArraySorted() {
        throw new UnsupportedOperationException();
    } // HARD! Traverse from left-most low to right-most low

    public int getTreeDepth(Node root) {
        return root == null ? 0 : (1 + Math.max(getTreeDepth(root.left), getTreeDepth(root.right)));
    }

    private void writeArray(Node currNode, int rowIndex, int columnIndex, String[][] res, int treeDepth) {
        // 保证输入的树不为空
        if (currNode == null) {
            return;
        }
        // 先将当前节点保存到二维数组中
        res[rowIndex][columnIndex] = String.valueOf(currNode.value);

        // 计算当前位于树的第几层
        int currLevel = ((rowIndex + 1) / 2);
        // 若到了最后一层，则返回
        if (currLevel == treeDepth) {
            return;
        }
        // 计算当前行到下一行，每个元素之间的间隔（下一行的列索引与当前元素的列索引之间的间隔）
        int gap = treeDepth - currLevel - 1;

        // 对左儿子进行判断，若有左儿子，则记录相应的"/"与左儿子的值
        if (currNode.left != null) {
            res[rowIndex + 1][columnIndex - gap] = "/";
            writeArray(currNode.left, rowIndex + 2, columnIndex - gap * 2, res, treeDepth);
        }

        // 对右儿子进行判断，若有右儿子，则记录相应的"\"与右儿子的值
        if (currNode.right != null) {
            res[rowIndex + 1][columnIndex + gap] = "\\";
            writeArray(currNode.right, rowIndex + 2, columnIndex + gap * 2, res, treeDepth);
        }
    }

    public void printDebug() {
        if (root == null) {
            System.out.println("EMPTY!");
        }
        // 得到树的深度
        int treeDepth = getTreeDepth(root);

        // 最后一行的宽度为2的（n - 1）次方乘3，再加1
        // 作为整个二维数组的宽度
        int arrayHeight = treeDepth * 2 - 1;
        int arrayWidth = (2 << (treeDepth - 2)) * 3 + 1;
        // 用一个字符串数组来存储每个位置应显示的元素
        String[][] res = new String[arrayHeight][arrayWidth];
        // 对数组进行初始化，默认为一个空格
        for (int i = 0; i < arrayHeight; i++) {
            for (int j = 0; j < arrayWidth; j++) {
                res[i][j] = " ";
            }
        }

        // 从根节点开始，递归处理整个树
        // res[0][(arrayWidth + 1)/ 2] = (char)(root.val + '0');
        writeArray(root, 0, arrayWidth / 2, res, treeDepth);

        // 此时，已经将所有需要显示的元素储存到了二维数组中，将其拼接并打印即可
        for (String[] line : res) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < line.length; i++) {
                sb.append(line[i]);
                if (line[i].length() > 1 && i <= line.length - 1) {
                    i += line[i].length() > 4 ? 2 : line[i].length() - 1;
                }
            }
            System.out.println(sb.toString());
        }

    } // any output that helps you with debugging, recursively visit all nodes of the trees

    @Override
    public String toString() {
        return String.format("[%s]", String.join(",", toArray()));
    } // result like for ArrayList: "[Value1,Value2,Value3]"
}
