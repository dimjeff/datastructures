/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day01arraylistownimpl;

import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author ipd
 */
public class CustomArrayOfIntsTest {
    
    public CustomArrayOfIntsTest() {
    }
    
   
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of size method, of class CustomArrayOfInts.
     */
    @Test
    public void testSize() {
        CustomArrayOfInts instance = new CustomArrayOfInts();
        instance.add(0);
        instance.add(1);
        instance.add(2);
        instance.add(3);
        instance.add(4);
        instance.deleteByIndex(1);
        instance.deleteByIndex(2);
        assertEquals("Content:",4,instance.size());
        int[] content = instance.getSlice(0, instance.size());
        assertArrayEquals("step 1 test: ",new int[]{0,2,4}, content);     
        
        //assertArrayEquals("step 2 test: ",new int[]{0,2,4}, content);
    }

/*
    @Test
    public void testAdd() {
        System.out.println("add");
        int value = 0;
        CustomArrayOfInts instance = new CustomArrayOfInts();
        instance.add(value);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }


    @Test
    public void testDeleteByIndex() {
        System.out.println("deleteByIndex");
        int index = 0;
        CustomArrayOfInts instance = new CustomArrayOfInts();
        instance.deleteByIndex(index);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }


    @Test
    public void testDeleteByValue() {
        System.out.println("deleteByValue");
        int value = 0;
        CustomArrayOfInts instance = new CustomArrayOfInts();
        instance.deleteByValue(value);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }


    @Test
    public void testInsertValueAtIndex() {
        System.out.println("insertValueAtIndex");
        int value = 0;
        int index = 0;
        CustomArrayOfInts instance = new CustomArrayOfInts();
        instance.insertValueAtIndex(value, index);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }


    @Test
    public void testClear() {
        System.out.println("clear");
        CustomArrayOfInts instance = new CustomArrayOfInts();
        instance.clear();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }


    @Test
    public void testGet() {
        System.out.println("get");
        int index = 0;
        CustomArrayOfInts instance = new CustomArrayOfInts();
        int expResult = 0;
        int result = instance.get(index);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }


    @Test
    public void testGetSlice() {
        System.out.println("getSlice");
        int startIdx = 0;
        int length = 0;
        CustomArrayOfInts instance = new CustomArrayOfInts();
        int[] expResult = null;
        int[] result = instance.getSlice(startIdx, length);
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }


    @Test
    public void testToString() {
        System.out.println("toString");
        CustomArrayOfInts instance = new CustomArrayOfInts();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
*/
    
}
